package com.joshuaella.wordlehelp

import com.joshuaella.wordlehelp.domain.SearchFilter
import com.joshuaella.wordlehelp.repo.WordRepository
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun wordRepositoryTest() {
       val wordRepo = WordRepository()
        val searchFilter = SearchFilter(
            contains = "cic"
        )
       val result = wordRepo.getWords(searchFilter = searchFilter);
        assertTrue(result.contains("click"));
    }
}