package com.joshuaella.wordlehelp.di

import com.joshuaella.wordlehelp.domain.IWordRepository
import com.joshuaella.wordlehelp.repo.WordRepository
import com.joshuaella.wordlehelp.search.SearchUseCases
import com.joshuaella.wordlehelp.usecase.words.GetWords
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object WordSearchModule {

    @Provides
    @Singleton
    fun provideWordRepository(): IWordRepository {
        return WordRepository()
    }

    @Provides
    @Singleton
    fun provideSearchUseCases(repository: IWordRepository): SearchUseCases {
        return SearchUseCases(
            searchWords = GetWords(repository = repository)
        )
    }
}