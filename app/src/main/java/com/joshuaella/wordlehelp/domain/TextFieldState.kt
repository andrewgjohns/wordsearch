package com.joshuaella.wordlehelp.domain

data class TextFieldState (
    val text: String = "",
    val hint: String = ""
)