package com.joshuaella.wordlehelp.domain

data class StringListState(
    val items: List<String> = listOf()
)
