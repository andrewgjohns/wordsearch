package com.joshuaella.wordlehelp.domain

interface IWordRepository {

    fun getWords(searchFilter: SearchFilter): List<String>

}