package com.joshuaella.wordlehelp.domain

data class SearchResultState (
    val text: String = "",
    val hint: String = "",
    val isHintVisible: Boolean = true
)