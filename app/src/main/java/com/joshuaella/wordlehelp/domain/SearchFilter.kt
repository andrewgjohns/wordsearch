package com.joshuaella.wordlehelp.domain

data class SearchFilter(
    val contains: String = "",
    val excludes: String = "",
    val pattern: String = "",
    val antiPatterns: List<String> = listOf()
)
