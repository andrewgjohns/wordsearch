package com.joshuaella.wordlehelp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.joshuaella.wordlehelp.search.components.SearchScreen
import com.joshuaella.wordlehelp.theme.WordleHelpTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContent {
            WordleHelpTheme {
                SearchScreen()
            }
        }

    }
}

