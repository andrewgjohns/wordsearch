package com.joshuaella.wordlehelp.search

sealed class SearchEvent {
    data class EnteredContains(val value: String): SearchEvent()
    object ClearContains : SearchEvent()

    data class EnteredExcludes(val value: String): SearchEvent()
    object ClearExcludes: SearchEvent()

    data class EnteredPattern(val value: String): SearchEvent()
    object ClearPattern: SearchEvent()

    data class EnteredAntiPattern(val value: String): SearchEvent()
    object ClearAntiPattern: SearchEvent()

    object AddAntiPattern: SearchEvent()
    data class RemovedAntiPattern(val value: String): SearchEvent()
}