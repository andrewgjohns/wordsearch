package com.joshuaella.wordlehelp.search.components


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.material.Icon
import androidx.compose.material.icons.filled.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp


@Composable
fun SearchItem(
    text: String,
    hint: String,
    onValueChanged: (String) -> Unit,
    onCleared: () -> Unit,
    onImeAction: () -> Unit,
    singleLine: Boolean = true,
    modifier: Modifier = Modifier,
    textStyle: TextStyle = TextStyle()
) {
    TextField(
        value = text,
        label = {
            Text(text = hint)
        },
        onValueChange = onValueChanged,
        singleLine = singleLine,
        textStyle = textStyle,
        modifier = Modifier
            .fillMaxWidth(),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = MaterialTheme.colors.surface.copy(alpha = 0.3f),
            textColor = MaterialTheme.colors.onPrimary,
            focusedLabelColor = MaterialTheme.colors.secondary,
            focusedIndicatorColor = MaterialTheme.colors.secondary,
            cursorColor = MaterialTheme.colors.secondary
        ),
        keyboardOptions = KeyboardOptions.Default.copy(
            capitalization = KeyboardCapitalization.None,
            autoCorrect = false,
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Next
        ),
        keyboardActions = KeyboardActions(
            onNext = { onImeAction() }
        ),
        trailingIcon = {
            IconButton(modifier =
                Modifier
                    .then(Modifier.size(24.dp)),
                onClick = onCleared) {
                Icon(
                    imageVector = Icons.Default.Backspace,
                    contentDescription = "Clear")
            }
        }
    )
}

@Composable
fun AntiSearchItem(
    text: String,
    hint: String,
    onValueChanged: (String) -> Unit,
    onAdd: () -> Unit,
    onCleared: () -> Unit,
    onImeAction: () -> Unit,
    singleLine: Boolean = true,
    modifier: Modifier = Modifier,
    textStyle: TextStyle = TextStyle()
) {
    TextField(
        value = text,
        label = {
            Text(text = hint)
        },
        onValueChange = onValueChanged,
        singleLine = singleLine,
        textStyle = textStyle,
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = MaterialTheme.colors.surface.copy(alpha = 0.3f),
            textColor = MaterialTheme.colors.onPrimary,
            focusedLabelColor = MaterialTheme.colors.secondary,
            focusedIndicatorColor = MaterialTheme.colors.secondary,
            cursorColor = MaterialTheme.colors.secondary
        ),
        modifier = Modifier
            .fillMaxWidth(),
        keyboardOptions = KeyboardOptions.Default.copy(
            capitalization = KeyboardCapitalization.None,
            autoCorrect = false,
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Search
        ),
        keyboardActions = KeyboardActions(
            onSearch = { onImeAction() }
        ),
        trailingIcon = {
            Row(
                modifier = Modifier.padding(0.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                IconButton(
                    modifier = Modifier.then(Modifier.size(24.dp)),
                    onClick = onCleared
                ) {
                    Icon(
                        imageVector = Icons.Default.Backspace,
                        contentDescription = "Clear"
                    )
                }
                IconButton(
                    modifier = Modifier
                        .padding(4.dp, 0.dp, 4.dp, 0.dp)
                        .then(Modifier.size(24.dp)),
                    onClick = onAdd
                ) {
                    Icon(
                        imageVector = Icons.Default.AddBox,
                        contentDescription = "Clear",
                        tint = MaterialTheme.colors.secondary
                    )
                }
            }
        }
    )
}

@Composable
fun AntiPattern(
    text1: String?,
    onRemove1: () -> Unit,
    text2: String?,
    onRemove2: () -> Unit,
    modifier: Modifier = Modifier,
    textStyle: TextStyle = TextStyle()
) {

    if (text1 != null) {
        Column(
            modifier = Modifier
                .background(MaterialTheme.colors.secondaryVariant)
                .fillMaxWidth(0.5f)
        ) {
            Row() {

                Text(
                    text = text1,
                    modifier = Modifier
                        .padding(5.dp)
                        .weight(1f),
                    style = textStyle
                )
                IconButton(
                    modifier = Modifier
                        .padding(5.dp)
                        .then(Modifier.size(24.dp)),
                    onClick = onRemove1
                ) {
                    Icon(
                        imageVector = Icons.Default.Delete,
                        contentDescription = "Clear"
                    )
                }
            }
        }
    }
    if (text2 != null)
    {
        Column(
            modifier = Modifier
                .background(MaterialTheme.colors.secondaryVariant)
                .fillMaxWidth(),
        ) {
            Row() {
                Text(
                    text = text2,
                    modifier = Modifier
                        .padding(5.dp)
                        .weight(1f),
                    style = textStyle
                )
                IconButton(
                    modifier = Modifier
                        .padding(5.dp)
                        .then(Modifier.size(24.dp)),
                    onClick = onRemove2
                ) {
                    Icon(
                        imageVector = Icons.Default.Delete,
                        contentDescription = "Clear"
                    )
                }
            }
        }
    }

}

@Composable
fun SearchButton(
    text: String,
    enabled: Boolean = false,
    onClicked: () -> Unit,
    modifier: Modifier = Modifier,
    textStyle: TextStyle = TextStyle()
) {
    Button(
        onClick = onClicked,
        modifier = modifier.padding(start = 5.dp, end = 5.dp),
        contentPadding = PaddingValues(16.dp),
        enabled = enabled,
        colors = ButtonDefaults.buttonColors(
            disabledBackgroundColor = MaterialTheme.colors.secondary.copy(alpha = 0.3f),
            disabledContentColor = MaterialTheme.colors.onPrimary.copy(alpha = 0.3f),
            contentColor = MaterialTheme.colors.onSecondary,
            backgroundColor = MaterialTheme.colors.secondary
        )
    ) {
        Text(
            text = text,
            style = textStyle
        )
    }
}
