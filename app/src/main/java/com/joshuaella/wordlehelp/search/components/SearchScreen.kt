package com.joshuaella.wordlehelp.search.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.joshuaella.wordlehelp.search.*
import androidx.compose.foundation.lazy.items

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchScreen(
    viewModel: SearchViewModel = hiltViewModel()
) {
    val containsState = viewModel.containsText.value
    val excludesState = viewModel.excludesText.value
    val patternState = viewModel.patternText.value
    val antiPatternState = viewModel.antiPatternText.value
    val antiPatternsState = viewModel.antiPatterns.value
    val resultState = viewModel.resultsText.value

    val scrollState = rememberScrollState()
    val localFocusManager = LocalFocusManager.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(0.dp),
        verticalArrangement = Arrangement.spacedBy(5.dp)
    ) {

        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(0.dp),
        ) {

            Column(
                modifier = Modifier.fillMaxWidth(0.45f),
                verticalArrangement = Arrangement.spacedBy(0.dp)

            ) {
                SearchItem(
                    text = containsState.text,
                    hint = containsState.hint,
                    onValueChanged = {
                        viewModel.onEvent(SearchEvent.EnteredContains(it))
                    },
                    onCleared = {
                        viewModel.onEvent(SearchEvent.ClearContains)
                    },
                    onImeAction = {
                        localFocusManager.moveFocus(FocusDirection.Down)
                    },
                    textStyle = MaterialTheme.typography.body1
                )
                SearchItem(
                    text = excludesState.text,
                    hint = excludesState.hint,
                    onValueChanged = {
                        viewModel.onEvent(SearchEvent.EnteredExcludes(it))
                    },
                    onCleared = {
                        viewModel.onEvent(SearchEvent.ClearExcludes)
                    },
                    onImeAction = {
                        localFocusManager.moveFocus(FocusDirection.Right)
                    },
                    textStyle = MaterialTheme.typography.body1
                )
            }

            Column(
                modifier = Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.spacedBy(0.dp)

            ) {

                SearchItem(
                    text = patternState.text,
                    hint = patternState.hint,
                    onValueChanged = {
                        viewModel.onEvent(SearchEvent.EnteredPattern(it))
                    },
                    onCleared = {
                        viewModel.onEvent(SearchEvent.ClearPattern)
                    },
                    onImeAction = {
                        localFocusManager.moveFocus(FocusDirection.Down)
                    },
                    textStyle = MaterialTheme.typography.body1
                )
                AntiSearchItem(
                    text = antiPatternState.text,
                    hint = antiPatternState.hint,
                    onValueChanged = {
                        viewModel.onEvent(SearchEvent.EnteredAntiPattern(it))
                    },
                    onCleared = {
                        viewModel.onEvent(SearchEvent.ClearAntiPattern)
                    },
                    onAdd = {
                        viewModel.onEvent(SearchEvent.AddAntiPattern)
                    },
                    onImeAction = {
                        localFocusManager.clearFocus()
                    },
                    textStyle = MaterialTheme.typography.body1
                )
            }

        }
        val itemSize = antiPatternsState.items.size
        var x = 0
        while (x < itemSize) {
            val text1 = antiPatternsState.items.get(x);
            x++;
            val text2 = if (x >= itemSize)  null else antiPatternsState.items.get(x)
            x++
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(5.dp, 0.dp, 5.dp, 0.dp),
                horizontalArrangement = Arrangement.spacedBy(5.dp),
            ) {
                AntiPattern(
                    text1 = text1,
                    onRemove1 = {
                        viewModel.onEvent(SearchEvent.RemovedAntiPattern(text1))
                    },
                    text2 = text2,
                    onRemove2 = {
                        if (text2 == null) {
                        } else viewModel.onEvent(SearchEvent.RemovedAntiPattern(text2))
                    },
                    textStyle = MaterialTheme.typography.body1
                )
            }
        }
        Row(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(scrollState),
            Arrangement.Center
        ) {

            if (resultState.isHintVisible) {
                Text(
                    text = resultState.hint,
                    style = MaterialTheme.typography.body1,
                    color = Color.DarkGray
                )
            } else {
                Text(
                    text = resultState.text,
                    Modifier.
                        padding(start = 10.dp)
                        .fillMaxSize(),
                    style = TextStyle(
                        fontFamily = FontFamily.Monospace,
                        fontSize = 16.sp,
                        color = MaterialTheme.colors.onPrimary
                    )
                )
            }
        }
    }
}
