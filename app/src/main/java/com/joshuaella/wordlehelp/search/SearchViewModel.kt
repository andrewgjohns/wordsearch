package com.joshuaella.wordlehelp.search

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
import com.joshuaella.wordlehelp.domain.*
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchUseCases: SearchUseCases
): ViewModel() {

    private val txtPattern = Regex("[^a-z]")
    private val patternPattern = Regex("[^a-z\\.]")

    private val _containsText = mutableStateOf(TextFieldState(
        hint = "contains: abc"
    ))
    val containsText: State<TextFieldState> = _containsText;

    private val _excludesText = mutableStateOf(TextFieldState(
        hint = "excludes: def"
    ))
    val excludesText: State<TextFieldState> = _excludesText;

    private val _patternText = mutableStateOf(TextFieldState(
        hint = "pattern: ..a.."
    ))
    val patternText: State<TextFieldState> = _patternText;

    private val _antiPatternText = mutableStateOf(TextFieldState(
        hint = "not pattern: ..i.."
    ))
    val antiPatternText: State<TextFieldState> = _antiPatternText;

    private val _antiPatterns = mutableStateOf(StringListState())
    val antiPatterns: State<StringListState> = _antiPatterns;

    private val _resultsText = mutableStateOf(SearchResultState(
        hint = "no results",
        isHintVisible = true
    ))
    val resultsText: State<SearchResultState> = _resultsText;

    fun onEvent(event: SearchEvent) {
        when(event) {
            is SearchEvent.EnteredContains -> {
                val newValue = textInput(event.value)
                if (newValue != _containsText.value.text) {
                    _containsText.value = containsText.value.copy(
                        text = newValue
                    )
                    search()
                }
            }
            is SearchEvent.ClearContains -> {
                val newValue = ""
                if (newValue != _containsText.value.text) {
                    _containsText.value = containsText.value.copy(
                        text = newValue
                    )
                    search()
                }
            }


            is SearchEvent.EnteredExcludes -> {
                val newValue = textInput(event.value)
                if (newValue != _excludesText.value.text) {
                    _excludesText.value = excludesText.value.copy(
                        text = newValue
                    )
                    search()
                }
            }
            is SearchEvent.ClearExcludes -> {
                val newValue = ""
                if (newValue != _excludesText.value.text) {
                    _excludesText.value = excludesText.value.copy(
                        text = newValue
                    )
                    search()
                }
            }

            is SearchEvent.EnteredPattern -> {
                val newValue = patternInput(event.value)
                if (newValue != _patternText.value.text) {
                    _patternText.value = patternText.value.copy(
                        text = newValue
                    )
                    search()
                }
            }
            is SearchEvent.ClearPattern -> {
                val newValue = ""
                if (newValue != _patternText.value.text) {
                    _patternText.value = patternText.value.copy(
                        text = newValue
                    )
                    search()
                }
            }

            is SearchEvent.EnteredAntiPattern -> {
                val newValue = patternInput(event.value)
                if (newValue != _antiPatternText.value.text) {
                    _antiPatternText.value = antiPatternText.value.copy(
                        text = newValue
                    )
                }
            }
            is SearchEvent.ClearAntiPattern -> {
                val newValue = ""
                if (newValue != _antiPatternText.value.text) {
                    _antiPatternText.value = antiPatternText.value.copy(
                        text = newValue
                    )
                    search()
                }
            }

            is SearchEvent.AddAntiPattern -> {
                addAntiPattern()
            }
            is SearchEvent.RemovedAntiPattern -> {
                removeAntiPattern(event.value);
            }
        }
    }

    private fun patternInput(value: String): String {
        var candidate = value.lowercase().take(5);
        return patternPattern.replace(candidate, "");
    }

    private fun textInput(value: String): String {
        var candidate = value.lowercase();
        return txtPattern.replace(candidate, "");
    }

    fun addAntiPattern() {
        val antiPattern = antiPatternText.value.text;
        val items = antiPatterns.value.items;
        val antiPatterns = mutableListOf<String>();
        antiPatterns.addAll(items)

        if (antiPattern.length == 5 && ! antiPatterns.contains(antiPattern)) {

            antiPatterns.add(antiPattern)
            _antiPatterns.value = _antiPatterns.value.copy(
                items = antiPatterns
            )
            _antiPatternText.value = antiPatternText.value.copy(
                text = "",
            )
            search()
        }
    }

    fun removeAntiPattern(item: String) {
        val items = antiPatterns.value.items;
        val antiPatterns = mutableListOf<String>();
        antiPatterns.addAll(items)

        if (item.isNotBlank() && antiPatterns.contains(item)) {

            antiPatterns.remove(item)
            _antiPatterns.value = _antiPatterns.value.copy(
                items = antiPatterns
            )
            search()
        }
    }

    fun search() {

        val pattern = if (_patternText.value.text.length == 5) _patternText.value.text else ""

        val enabled: Boolean = _containsText.value.text.isNotBlank()
                || _excludesText.value.text.isNotBlank()
                || pattern.isNotBlank()
                || _antiPatterns.value.items.isNotEmpty()
        if (enabled) {

            val result = searchUseCases.searchWords(
                searchFilter = SearchFilter(
                    contains = containsText.value.text,
                    excludes = excludesText.value.text,
                    pattern = pattern,
                    antiPatterns = antiPatterns.value.items
                )
            )
            setResult(result)
        } else {
            setResult(listOf())
        }
    }

    private fun setResult(result: List<String>) {
        if (result.isNotEmpty()) {
            _resultsText.value = resultsText.value.copy(
                isHintVisible = false,
                text = result.joinToString(
                    separator = "   "
                )
            )

        } else {
            _resultsText.value = resultsText.value.copy(
                isHintVisible = true,
                hint = "No Results"
            )
        }
    }
}

