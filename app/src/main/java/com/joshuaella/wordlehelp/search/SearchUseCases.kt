package com.joshuaella.wordlehelp.search

import com.joshuaella.wordlehelp.usecase.words.GetWords

data class SearchUseCases(
    val searchWords: GetWords
)
