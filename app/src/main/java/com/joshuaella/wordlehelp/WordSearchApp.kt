package com.joshuaella.wordlehelp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WordSearchApp : Application()