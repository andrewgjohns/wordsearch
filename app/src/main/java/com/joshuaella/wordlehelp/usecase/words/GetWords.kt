package com.joshuaella.wordlehelp.usecase.words

import com.joshuaella.wordlehelp.domain.IWordRepository
import com.joshuaella.wordlehelp.domain.SearchFilter
import kotlinx.coroutines.flow.Flow

class GetWords (private val repository: IWordRepository) {

    operator fun invoke(
        searchFilter: SearchFilter = SearchFilter()
    ) : List<String> {
        return repository.getWords(searchFilter);
    }
}